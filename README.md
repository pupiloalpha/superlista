SuperLista
==========

Aplicativo para plataforma Android que ajuda na gestão de listas


Notas das versões desenvolvidas

1.0 - Cria listas e coloca itens de uma categoria ou digitadas;
1.1 - Envia as listas por e-mail ou mensagem de texto;
1.2 - Ambiente de utilização da lista com abas;
1.3 - Novo visual do aplicativo;
1.4 - Suporte para backup no Cartão SD;
1.5.1 - Disponível no AndroidMarcket;

