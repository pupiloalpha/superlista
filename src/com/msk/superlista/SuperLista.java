package com.msk.superlista;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.msk.superlista.db.DBListas;

public class SuperLista extends ActionBarActivity implements View.OnClickListener {
	private Button criaLista;
	private DBListas dbLista = new DBListas(this);
	private Button escolheLista;
	private String nomeDaLista;
	private EditText nomeLista;
	
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.inicio);
		iniciar();
		usarActionBar();
		criaLista.setOnClickListener(this);
		escolheLista.setOnClickListener(this);
	}

	private void iniciar() {
		criaLista = ((Button) findViewById(R.id.bCriaLista));
		escolheLista = ((Button) findViewById(R.id.bEscolheLista));
		nomeLista = ((EditText) findViewById(R.id.etNomeLista));
	}

	public void onClick(View paramView) {
		switch (paramView.getId()) {
		case R.id.bCriaLista:

			if (nomeLista.getText().toString().equals(""))
				nomeDaLista = "Sem Nome";
			else
				nomeDaLista = nomeLista.getText().toString();

			String nomeLista1 = nomeDaLista;
			String nomeLista2 = nomeDaLista;
			dbLista.open();
			int a = dbLista.contaNomeListas(nomeLista1);
			int b = 1;

			if (a != 0) {
				while (a != 0) {
					nomeLista2 = nomeLista1 + b;
					a = dbLista.contaNomeListas(nomeLista2);
					b = b + 1;
				}
				nomeDaLista = nomeLista2;
			}

			dbLista.criaLista(nomeDaLista);
			dbLista.close();
			Bundle localBundle = new Bundle();
			localBundle.putString("lista", nomeDaLista);
			Intent localIntent = new Intent("com.msk.superlista.CRIALISTA");
			localIntent.putExtras(localBundle);
			startActivity(localIntent);
			nomeLista.setText("");
			break;
		case R.id.bEscolheLista:

			startActivity(new Intent("com.msk.superlista.ESCOLHELISTA"));
			nomeLista.setText("");

			break;
		}
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(false);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu paramMenu) {
		super.onCreateOptionsMenu(paramMenu);
		getMenuInflater().inflate(R.menu.menu_inicio, paramMenu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
		switch (paramMenuItem.getItemId()) {

		case R.id.ajustes:
			startActivity(new Intent("com.msk.superlista.AJUSTES"));
			break;
		case R.id.sobre:
			startActivity(new Intent("com.msk.superlista.SOBRE"));
			break;
		case R.id.sair:
			finish();
			break;
		}
		return false;

	}
}