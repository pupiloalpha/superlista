package com.msk.superlista.db;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.msk.superlista.R;
import com.msk.superlista.SuperLista;

@SuppressWarnings("deprecation")
public class UsaListas extends SuperLista implements
		View.OnClickListener {

	// ELEMENTOS DA TELA
	private Button bCestaCheia, bCestaVazia;
	private ImageView ivExclui;
	private LayoutInflater inflaterLista;
	private ListView listaCestaCheia, listaCestaVazia;
	private TextView nomeItem, semItens, nomeLista, valorLista, preco, unidade;

	// VARIAREIS UTILIZADAS
	private String listaFeita, itemDaLista, quantidadeItem, valorItem;
	private Cursor itensCestaCheia = null;
	private Cursor itensCestaVazia = null;
	private SimpleCursorAdapter buscaLista, buscaCesta;
	private View viewLista;
	private Long idItem;

	// CLASSE COM BANCO DE DADOS
	private DBListas dbListaCriada = new DBListas(this);

	// PREFRENCIAS DO USUARIO
	SharedPreferences buscaPreferencias = null;
	Boolean apagarLista = true;

	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.usa_lista);
		buscaPreferencias = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		apagarLista = buscaPreferencias.getBoolean("cesta", true);
		iniciar();
		listaFeita = getIntent().getExtras().getString("lista");
		usarActionBar();
		nomeLista.setText(listaFeita);
		CestaCheia();
		CestaVazia();
		listaCestaVazia.setOnItemClickListener(itemCestaVazia);
		listaCestaVazia.setOnItemLongClickListener(pressaoItemCestaVazia);
		listaCestaCheia.setOnItemClickListener(itemCestaCheia);
		listaCestaCheia.setOnItemLongClickListener(pressaoItemCestaCheia);
		listaCestaCheia.setVisibility(View.GONE);

	}

	private void iniciar() {
		nomeLista = ((TextView) findViewById(R.id.tvNomeLista));
		listaCestaVazia = ((ListView) findViewById(R.id.lvCestaVazia));
		listaCestaCheia = ((ListView) findViewById(R.id.lvCestaCheia));
		semItens = (TextView) findViewById(R.id.tvCestaSemItem);
		valorLista = (TextView) findViewById(R.id.tvValorLista);
		bCestaVazia = ((Button) findViewById(R.id.bCestaVazia));
		bCestaCheia = ((Button) findViewById(R.id.bCestaCheia));
		bCestaVazia.setOnClickListener(this);
		bCestaCheia.setOnClickListener(this);

	}

	private void CestaCheia() {
		dbListaCriada.open();
		itensCestaCheia = dbListaCriada.buscaItensCesta(listaFeita);
		int e = itensCestaCheia.getCount();
		dbListaCriada.close();
		if (e >= 0) { // VERIFICA CURSOR NULO
			buscaCesta = new SimpleCursorAdapter(this, R.id.lvCestaCheia,
					itensCestaCheia, new String[] { "item_cesta",
							"qte_item_cesta", "preco_item_cesta" }, new int[] {
							R.id.tvItemCesta, R.id.tvUnid, R.id.tvPreco }) {
				public int getCount() {
					dbListaCriada.open();
					int d = dbListaCriada.contaItensCesta(listaFeita);
					dbListaCriada.close();
					return d;
				}

				public long getItemId(int pitemCestaCheia) {
					return pitemCestaCheia;
				}

				public View getView(int pitemCestaCheia, View vCestaCheia,
						ViewGroup vGCestaCheia) {
					viewLista = vCestaCheia;
					inflaterLista = getLayoutInflater();
					viewLista = inflaterLista.inflate(R.layout.item_na_cesta,
							null);
					nomeItem = ((TextView) viewLista
							.findViewById(R.id.tvItemCesta));
					unidade = ((TextView) viewLista.findViewById(R.id.tvUnid));
					preco = ((TextView) viewLista.findViewById(R.id.tvPreco));
					ivExclui = (ImageView) viewLista.findViewById(R.id.ivApaga);
					dbListaCriada.open();
					itensCestaCheia.moveToPosition(pitemCestaCheia);
					itemDaLista = itensCestaCheia.getString(2);
					quantidadeItem = itensCestaCheia.getString(3);
					valorItem = itensCestaCheia.getString(4);
					ivExclui.setTag(itensCestaCheia.getLong(0));
					nomeItem.setText(itemDaLista);
					unidade.setText(quantidadeItem);
					preco.setText(valorItem);
					dbListaCriada.close();

					ivExclui.setOnClickListener(new View.OnClickListener() {
						public void onClick(View vItem) {
							ivExclui = ((ImageView) vItem);
							idItem = ((Long) ivExclui.getTag()).longValue();
							dbListaCriada.open();
							String str = dbListaCriada.mostraItemCesta(idItem);

							if (dbListaCriada.excluiItemCesta(idItem) == true)
								Toast.makeText(
										getApplicationContext(),
										String.format(
												getResources()
														.getString(
																R.string.dica_item_removido),
												str), Toast.LENGTH_SHORT)
										.show();

							dbListaCriada.close();
							buscaCesta.notifyDataSetChanged();
							CestaCheia();
						}
					});

					return viewLista;
				}
			};
			listaCestaCheia.setAdapter(buscaCesta);
			listaCestaCheia.setEmptyView(semItens);
			mostraValor(listaFeita);
		}
	}

	private void CestaVazia() {
		dbListaCriada.open();
		itensCestaVazia = dbListaCriada.buscaItensLista(listaFeita);
		int u = itensCestaVazia.getCount();
		dbListaCriada.close();

		if (u >= 0) { // VERIFICA CURSOR NULO
			buscaLista = new SimpleCursorAdapter(this, R.id.lvCestaVazia,
					itensCestaVazia, new String[] { "item_lista",
							"qte_item_lista", "preco_item_lista" }, new int[] {
							R.id.tvItemCesta, R.id.tvUnid, R.id.tvPreco }) {
				public int getCount() {
					dbListaCriada.open();
					int i = dbListaCriada.contaItensLista(listaFeita);
					dbListaCriada.close();
					return i;
				}

				public long getItemId(int pItemCestaVazia) {
					return pItemCestaVazia;
				}

				public View getView(int pItemCestaVazia, View vCestaVazia,
						ViewGroup vGCestaVazia) {
					viewLista = vCestaVazia;
					inflaterLista = getLayoutInflater();
					viewLista = inflaterLista.inflate(R.layout.item_na_cesta,
							null);
					nomeItem = ((TextView) viewLista
							.findViewById(R.id.tvItemCesta));
					unidade = ((TextView) viewLista.findViewById(R.id.tvUnid));
					preco = ((TextView) viewLista.findViewById(R.id.tvPreco));
					ivExclui = (ImageView) viewLista.findViewById(R.id.ivApaga);
					dbListaCriada.open();
					itensCestaVazia.moveToPosition(pItemCestaVazia);
					itemDaLista = itensCestaVazia.getString(2);
					quantidadeItem = itensCestaVazia.getString(3);
					valorItem = itensCestaVazia.getString(4);
					ivExclui.setTag(itensCestaVazia.getLong(0));
					nomeItem.setText(itemDaLista);
					unidade.setText(quantidadeItem);
					preco.setText(valorItem);
					dbListaCriada.close();

					ivExclui.setOnClickListener(new View.OnClickListener() {
						public void onClick(View vItem) {
							ivExclui = ((ImageView) vItem);
							idItem = ((Long) ivExclui.getTag()).longValue();
							dbListaCriada.open();
							String str = dbListaCriada.mostraItemLista(idItem);

							if (dbListaCriada.excluiItemLista(idItem) == true)
								Toast.makeText(
										getApplicationContext(),
										String.format(
												getResources()
														.getString(
																R.string.dica_item_removido),
												str), Toast.LENGTH_SHORT)
										.show();

							dbListaCriada.close();
							buscaLista.notifyDataSetChanged();
							CestaVazia();
						}
					});

					return viewLista;
				}
			};
			listaCestaVazia.setAdapter(buscaLista);
			listaCestaVazia.setEmptyView(semItens);
			mostraValor(listaFeita);
		}
	}

	private void mostraValor(String lista) {
		dbListaCriada.open();
		String str = dbListaCriada.mostraValorCesta(lista);
		dbListaCriada.close();
		valorLista.setText(getResources().getString(R.string.info_valor_item,
				str));
	}

	public void onClick(View paramView) {
		switch (paramView.getId()) {
		case R.id.bCestaVazia:
			bCestaVazia.setBackgroundColor(Color.rgb(51, 181, 229));
			bCestaVazia.setTextColor(-1);
			bCestaCheia.setBackgroundColor(0);
			bCestaCheia.setTextColor(-16777216);
			listaCestaCheia.setVisibility(View.GONE);
			listaCestaVazia.setVisibility(View.VISIBLE);
			CestaVazia();
			break;

		case R.id.bCestaCheia:
			bCestaCheia.setBackgroundColor(Color.rgb(51, 181, 229));
			bCestaCheia.setTextColor(-1);
			bCestaVazia.setBackgroundColor(0);
			bCestaVazia.setTextColor(-16777216);
			listaCestaVazia.setVisibility(View.GONE);
			listaCestaCheia.setVisibility(View.VISIBLE);
			CestaCheia();
			break;
		}
	}

	OnItemClickListener itemCestaCheia = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int posicao,
				long arg3) {
			// METODO DO ITEM DA LISTA CHEIA
			dbListaCriada.open();
			itensCestaCheia = dbListaCriada.buscaItensCesta(listaFeita);
			itensCestaCheia.moveToPosition(posicao);

			// BUSCA DADOS DO ITEM
			idItem = itensCestaCheia.getLong(0);
			itemDaLista = itensCestaCheia.getString(2);
			quantidadeItem = itensCestaCheia.getString(3);
			valorItem = itensCestaCheia.getString(4);

			// MUDA O ITEM DE UMA LISTA PARA OUTRA
			dbListaCriada.excluiItemCesta(idItem);
			dbListaCriada.insereItemLista(listaFeita, itemDaLista,
					quantidadeItem, valorItem);
			dbListaCriada.close();
			Toast.makeText(
					getApplicationContext(),
					String.format(
							getResources().getString(
									R.string.dica_item_fora_cesta), itemDaLista),
					Toast.LENGTH_SHORT).show();
			CestaCheia();
		}

	};

	OnItemClickListener itemCestaVazia = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> arg0, View arg1, int posicao,
				long arg3) {
			// METODO DO ITEM DA LISTA VAZIA
			dbListaCriada.open();
			itensCestaVazia = dbListaCriada.buscaItensLista(listaFeita);
			itensCestaVazia.moveToPosition(posicao);

			// BUSCA DADOS DO ITEM
			idItem = itensCestaVazia.getLong(0);
			itemDaLista = itensCestaVazia.getString(2);
			quantidadeItem = itensCestaVazia.getString(3);
			valorItem = itensCestaVazia.getString(4);

			// MUDA O ITEM DE UMA LISTA PARA OUTRA
			dbListaCriada.excluiItemLista(idItem);
			dbListaCriada.insereItemCesta(listaFeita, itemDaLista,
					quantidadeItem, valorItem);
			dbListaCriada.close();
			Toast.makeText(
					getApplicationContext(),
					String.format(
							getResources().getString(
									R.string.dica_item_na_cesta), itemDaLista),
					Toast.LENGTH_SHORT).show();
			CestaVazia();
		}

	};

	// METODO MANTEM ITEM DA LISTA PRESSIONADO
	OnItemLongClickListener pressaoItemCestaVazia = new OnItemLongClickListener() {

		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			dbListaCriada.open();
			itensCestaVazia = dbListaCriada.buscaItensLista(listaFeita);

			// BUSCA DADOS DO ITEM
			itensCestaVazia.moveToPosition(position);
			Long id = itensCestaVazia.getLong(0);
			String str = itensCestaVazia.getString(2);

			// ENVIA DADOS PARA EDITOR DO ITEM
			Bundle carta = new Bundle();
			carta.putString("item", str);
			carta.putLong("id", id);
			carta.putString("cesta", "vazia");
			Intent correio = new Intent("com.msk.superlista.EDITAITEMCESTA");
			correio.putExtras(carta);
			startActivityForResult(correio, 0);

			dbListaCriada.close();
			return true;
		}

	};

	// METODO MANTEM ITEM DA LISTA PRESSIONADO
	OnItemLongClickListener pressaoItemCestaCheia = new OnItemLongClickListener() {

		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int position, long arg3) {
			dbListaCriada.open();
			itensCestaCheia = dbListaCriada.buscaItensCesta(listaFeita);

			// BUSCA DADOS DO ITEM
			itensCestaCheia.moveToPosition(position);
			Long id = itensCestaCheia.getLong(0);
			String str = itensCestaCheia.getString(2);

			// ENVIA DADOS PARA EDITOR DO ITEM
			Bundle carta = new Bundle();
			carta.putString("item", str);
			carta.putLong("id", id);
			carta.putString("cesta", "cheia");
			Intent correio = new Intent("com.msk.superlista.EDITAITEMCESTA");
			correio.putExtras(carta);
			startActivityForResult(correio, 0);

			dbListaCriada.close();
			return true;
		}

	};

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(listaFeita);
		nomeLista.setVisibility(View.GONE);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		getMenuInflater().inflate(R.menu.menu_usa_lista, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		case R.id.botao_novo_item:
			Bundle carta = new Bundle();
			carta.putString("lista", listaFeita);
			if (listaCestaVazia.getVisibility() == View.GONE) {
				carta.putString("cesta", "cheia");
			} else if (listaCestaCheia.getVisibility() == View.GONE) {
				carta.putString("cesta", "vazia");
			}
			Intent correio = new Intent("com.msk.superlista.NOVOITEM");
			correio.putExtras(carta);
			startActivityForResult(correio, 0);
			break;
		case R.id.botao_refaz_lista:
			dbListaCriada.open();
			itensCestaCheia = dbListaCriada.buscaItensCesta(listaFeita);

			for (int j = 0; j < itensCestaCheia.getCount(); j++) {
				itensCestaCheia.moveToPosition(j);
				itemDaLista = itensCestaCheia.getString(2);
				quantidadeItem = itensCestaCheia.getString(3);
				valorItem = itensCestaCheia.getString(4);
				dbListaCriada.insereItemLista(listaFeita, itemDaLista,
						quantidadeItem, valorItem);
			}

			dbListaCriada.excluiItensCesta(listaFeita);
			buscaCesta.notifyDataSetChanged();
			buscaLista.notifyDataSetChanged();
			CestaCheia();
			CestaVazia();
			dbListaCriada.close();
			break;
		case R.id.ajustes:
			startActivity(new Intent("com.msk.superlista.AJUSTES"));
			break;
		case R.id.sobre:
			startActivity(new Intent("com.msk.superlista.SOBRE"));
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void onDestroy() {
		super.onDestroy();
		dbListaCriada.open();
		itensCestaCheia = dbListaCriada.buscaItensLista(listaFeita);
		if (itensCestaCheia.getCount() == 0 && apagarLista == true) {
			dbListaCriada.excluiItensCesta(listaFeita);
			dbListaCriada.excluiItensLista(listaFeita);
			dbListaCriada.excluiLista(listaFeita);
		}
		dbListaCriada.close();
	}

	@Override
	protected void onPause() {

		if (listaCestaVazia.getVisibility() == View.GONE) {
			CestaCheia();
		} else if (listaCestaCheia.getVisibility() == View.GONE) {
			CestaVazia();
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (listaCestaVazia.getVisibility() == View.GONE) {
			CestaCheia();
		} else if (listaCestaCheia.getVisibility() == View.GONE) {
			CestaVazia();
		}
		super.onResume();
	}

}
