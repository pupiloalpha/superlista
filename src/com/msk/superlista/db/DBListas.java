package com.msk.superlista.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class DBListas {

	// Valores para criar o Banco de Dados
	private static final String BANCO_DE_DADOS = "super_lista";
	private static final int VERSAO_BANCO_DE_DADOS = 1;
	private static final String TABELA_ITENS_CESTA = "itens_da_cesta";
	private static final String TABELA_ITENS_LISTA = "itens_da_lista";
	private static final String TABELA_LISTAS = "listas";
	private static final String TAG = "DBListas";

	// Nomes das Colunas do Banco de Dados
	public static final String COLUNA_CODIGO_CESTA = "id_cesta";
	public static final String COLUNA_CODIGO_LISTA = "id_lista";
	public static final String COLUNA_DESCRICAO_ITEM_CESTA = "item_cesta";
	public static final String COLUNA_DESCRICAO_ITEM_LISTA = "item_lista";
	public static final String COLUNA_DESCRICAO_LISTA = "lista";
	public static final String COLUNA_ID_ITEM_CESTA = "_id";
	public static final String COLUNA_ID_ITEM_LISTA = "_id";
	public static final String COLUNA_ID_LISTA = "_id";
	public static final String COLUNA_PRECO_ITEM_CESTA = "preco_item_cesta";
	public static final String COLUNA_PRECO_ITEM_LISTA = "preco_item_lista";
	public static final String COLUNA_QUANTIDADE_ITEM_CESTA = "qte_item_cesta";
	public static final String COLUNA_QUANTIDADE_ITEM_LISTA = "qte_item_lista";

	// Comandos SQL para criar as Tabelas do Banco de Dados

	private static final String CRIA_TABELA_ITENS_LISTA = "CREATE TABLE "
			+ TABELA_ITENS_LISTA + " ( " + COLUNA_ID_ITEM_LISTA
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUNA_CODIGO_LISTA
			+ " TEXT NOT NULL, " + COLUNA_DESCRICAO_ITEM_LISTA
			+ " TEXT NOT NULL, " + COLUNA_QUANTIDADE_ITEM_LISTA
			+ " TEXT NOT NULL, " + COLUNA_PRECO_ITEM_LISTA + " TEXT NOT NULL)";

	private static final String CRIA_TABELA_ITENS_CESTA = "CREATE TABLE "
			+ TABELA_ITENS_CESTA + " ( " + COLUNA_ID_ITEM_CESTA
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUNA_CODIGO_CESTA
			+ " TEXT NOT NULL, " + COLUNA_DESCRICAO_ITEM_CESTA
			+ " TEXT NOT NULL, " + COLUNA_QUANTIDADE_ITEM_CESTA
			+ " TEXT NOT NULL, " + COLUNA_PRECO_ITEM_CESTA + " TEXT NOT NULL)";

	private static final String CRIA_TABELA_LISTA = "CREATE TABLE "
			+ TABELA_LISTAS + " (" + COLUNA_ID_LISTA
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUNA_DESCRICAO_LISTA
			+ " TEXT NOT NULL, UNIQUE (" + COLUNA_DESCRICAO_LISTA
			+ ") ON CONFLICT REPLACE)";

	// Titulos das colunas das Tabelas do Banco de Dados
	static String[] colunas_itens_cesta = { COLUNA_ID_ITEM_CESTA,
			COLUNA_CODIGO_CESTA, COLUNA_DESCRICAO_ITEM_CESTA,
			COLUNA_QUANTIDADE_ITEM_CESTA, COLUNA_PRECO_ITEM_CESTA };
	static String[] colunas_itens_lista = { COLUNA_ID_ITEM_LISTA,
			COLUNA_CODIGO_LISTA, COLUNA_DESCRICAO_ITEM_LISTA,
			COLUNA_QUANTIDADE_ITEM_LISTA, COLUNA_PRECO_ITEM_LISTA };
	static String[] colunas_listas = { COLUNA_ID_LISTA, COLUNA_DESCRICAO_LISTA };

	private DatabaseHelper DBHelper;
	private final Context context;
	private SQLiteDatabase db;

	public DBListas(Context paramContext) {
		this.context = paramContext;
		this.DBHelper = new DatabaseHelper(this.context);
	}

	public DBListas open() throws SQLException {
		this.db = this.DBHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		this.DBHelper.close();
		this.db.close();
	}

	// ------- CURSORES QUE PERCORREM O BANCO DE DADOS EM BUSCA DE INFORMACOES

	public Cursor buscaItensCesta(String lista) {
		lista = lista.replace("'", "''");
		return this.db.query(TABELA_ITENS_CESTA, colunas_itens_cesta,
				COLUNA_CODIGO_CESTA + " = '" + lista + "' ", null, null, null,
				COLUNA_DESCRICAO_ITEM_CESTA + " ASC");
	}

	public Cursor buscaItensLista(String lista) {
		lista = lista.replace("'", "''");
		return this.db.query(TABELA_ITENS_LISTA, colunas_itens_lista,
				COLUNA_CODIGO_LISTA + " = '" + lista + "' ", null, null, null,
				COLUNA_DESCRICAO_ITEM_LISTA + " ASC");
	}

	public Cursor buscaListas() {
		return this.db.query(TABELA_LISTAS, colunas_listas, null, null, null,
				null, COLUNA_DESCRICAO_LISTA + " ASC");
	}

	// ------METODOS QUE CONTAS VALORES NO BANCO DE DADOS

	public int contaItensCesta(String lista) {
		lista = lista.replace("'", "''");
		return this.db.query(TABELA_ITENS_CESTA, colunas_itens_cesta,
				COLUNA_CODIGO_CESTA + " = '" + lista + "' ", null, null, null,
				null).getCount();
	}

	public int contaItensLista(String lista) {
		lista = lista.replace("'", "''");
		return this.db.query(TABELA_ITENS_LISTA, colunas_itens_lista,
				COLUNA_CODIGO_LISTA + " = '" + lista + "' ", null, null, null,
				null).getCount();
	}

	public int contaListas() {
		return this.db.query(true, TABELA_LISTAS, colunas_listas, null, null,
				null, null, null, null).getCount();
	}

	public int contaNomeListas(String lista) {
		lista = lista.replace("'", "''");
		return this.db.query(true, TABELA_LISTAS, colunas_listas,
				COLUNA_DESCRICAO_LISTA + " = '" + lista + "' ", null, null,
				null, null, null).getCount();
	}

	public int contaNomeItensListas(String lista, String item) {
		lista = lista.replace("'", "''");
		item = item.replace("'", "''");
		return this.db.query(
				true,
				TABELA_ITENS_LISTA,
				colunas_itens_lista,
				COLUNA_CODIGO_LISTA + " = '" + lista + "' AND "
						+ COLUNA_DESCRICAO_ITEM_LISTA + " = '" + item + "' ",
				null, null, null, null, null).getCount();
	}

	public int contaNomeItensCestas(String lista, String item) {
		lista = lista.replace("'", "''");
		item = item.replace("'", "''");
		return this.db.query(
				true,
				TABELA_ITENS_CESTA,
				colunas_itens_cesta,
				COLUNA_CODIGO_CESTA + " = '" + lista + "' AND "
						+ COLUNA_DESCRICAO_ITEM_CESTA + " = '" + item + "' ",
				null, null, null, null, null).getCount();
	}
	
	// ------ METODOS QUE EXCLUEM VALORES NO BANCO DE DADOS

	public boolean excluiItem(String lista, String item) {
		lista = lista.replace("'", "''");
		item = item.replace("'", "''");
		return this.db.delete(TABELA_ITENS_LISTA, COLUNA_CODIGO_LISTA + " = '"
				+ lista + "' AND " + COLUNA_DESCRICAO_ITEM_LISTA + "= '" + item
				+ "'", null) > 0;
	}

	public boolean excluiItemCesta(long idLong) {
		return this.db.delete(TABELA_ITENS_CESTA, COLUNA_ID_ITEM_CESTA + " = '"
				+ idLong + "'", null) > 0;
	}

	public boolean excluiItemLista(long idLong) {
		return this.db.delete(TABELA_ITENS_LISTA, COLUNA_ID_ITEM_LISTA + " = '"
				+ idLong + "'", null) > 0;
	}

	public boolean excluiItensCesta(String lista) {
		lista = lista.replace("'", "''");
		return this.db.delete(TABELA_ITENS_CESTA, COLUNA_CODIGO_CESTA + " = '"
				+ lista + "' ", null) > 0;
	}

	public boolean excluiItensLista(String lista) {
		lista = lista.replace("'", "''");
		return this.db.delete(TABELA_ITENS_LISTA, COLUNA_CODIGO_LISTA + " = '"
				+ lista + "' ", null) > 0;
	}

	public boolean excluiLista(String lista) {
		lista = lista.replace("'", "''");
		return this.db.delete(TABELA_LISTAS, COLUNA_DESCRICAO_LISTA + " = '"
				+ lista + "' ", null) > 0;
	}

	public void excluiListas() {
		this.db.delete(TABELA_ITENS_CESTA, null, null);
		this.db.delete(TABELA_ITENS_LISTA, null, null);
		this.db.delete(TABELA_LISTAS, null, null);
	}

	// ------ METODOS QUE CRIAM VALORES NO BANCO DE DADOS

	public long insereItemCesta(String lista, String item, String quantidade,
			String preco) {
		ContentValues cv = new ContentValues();
		cv.put(COLUNA_CODIGO_CESTA, lista);
		cv.put(COLUNA_DESCRICAO_ITEM_CESTA, item);
		cv.put(COLUNA_QUANTIDADE_ITEM_CESTA, quantidade);
		cv.put(COLUNA_PRECO_ITEM_CESTA, preco);
		return this.db.insert(TABELA_ITENS_CESTA, null, cv);
	}

	public long insereItemLista(String lista, String item, String quantidade,
			String preco) {
		ContentValues cv = new ContentValues();
		cv.put(COLUNA_CODIGO_LISTA, lista);
		cv.put(COLUNA_DESCRICAO_ITEM_LISTA, item);
		cv.put(COLUNA_QUANTIDADE_ITEM_LISTA, quantidade);
		cv.put(COLUNA_PRECO_ITEM_LISTA, preco);
		return this.db.insert(TABELA_ITENS_LISTA, null, cv);
	}

	public long criaLista(String lista) {
		ContentValues cv = new ContentValues();
		cv.put(COLUNA_DESCRICAO_LISTA, lista);
		return this.db.insert(TABELA_LISTAS, null, cv);
	}

	// ------ METODOS QUE EXIBEM VALORES DO BANCO DE DADOS

	public String mostraItemCesta(long idLong) throws SQLException {
		Cursor cursor = this.db.query(TABELA_ITENS_CESTA, colunas_itens_cesta,
				COLUNA_ID_ITEM_CESTA + " = '" + idLong + "' ", null, null,
				null, null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(2);
		}
		cursor.close();
		return null;
	}

	public String mostraItemLista(long idLong) throws SQLException {
		Cursor cursor = this.db.query(TABELA_ITENS_LISTA, colunas_itens_lista,
				COLUNA_ID_ITEM_LISTA + " = '" + idLong + "' ", null, null,
				null, null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(2);
		}
		cursor.close();
		return null;
	}

	public String mostraQuantidadeItemCesta(long idLong) throws SQLException {
		Cursor cursor = this.db.query(TABELA_ITENS_CESTA, colunas_itens_cesta,
				COLUNA_ID_ITEM_CESTA + " = '" + idLong + "' ", null, null,
				null, null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(3);
		}
		cursor.close();
		return null;
	}

	public String mostraQuantidadeItemLista(long idLong) throws SQLException {
		Cursor cursor = this.db.query(TABELA_ITENS_LISTA, colunas_itens_lista,
				COLUNA_ID_ITEM_LISTA + " = '" + idLong + "' ", null, null,
				null, null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(3);
		}
		cursor.close();
		return null;
	}

	public String mostraValorItemCesta(long idLong) throws SQLException {
		Cursor cursor = this.db.query(TABELA_ITENS_CESTA, colunas_itens_cesta,
				COLUNA_ID_ITEM_CESTA + " = '" + idLong + "' ", null, null,
				null, null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(4);
		}
		cursor.close();
		return null;
	}

	public String mostraValorItemLista(long idLong) throws SQLException {
		Cursor cursor = this.db.query(TABELA_ITENS_LISTA, colunas_itens_lista,
				COLUNA_ID_ITEM_LISTA + " = '" + idLong + "' ", null, null,
				null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			return cursor.getString(4);
		}
		return null;
	}

	@SuppressLint("DefaultLocale")
	public String mostraValorCesta(String lista) throws SQLException {
		lista = lista.replace("'", "''");
		String u = "";
		String valor = "";
		String qte = "";
		Cursor cursor = this.db.query(TABELA_ITENS_CESTA, colunas_itens_cesta,
				COLUNA_CODIGO_CESTA + " = '" + lista + "' ", null, null, null,
				null);
		int i = cursor.getCount();
		cursor.moveToLast();
		double preco = 0.0D;
		double qt = 0.0D;
		for (int j = 0; j < i; j++) {
			valor = cursor.getString(4);
			qte = cursor.getString(3);
			qte = qte.replace("", "");
			qte = qte.replace(" unid", "");
			qte = qte.replace(" caixa", "");
			qte = qte.replace(" kg", "");
			qte = qte.replace(" litro", "");
			qte = qte.replace(" g", "");
			qte = qte.replace(" ml", "");
			qte = qte.replace(" pc", "");
			qte = qte.replace(",", ".");
			qt = Double.parseDouble(qte);
			valor = valor.replace("R$ ", "");
			valor = valor.replace(",", ".");
			preco += qt * Double.parseDouble(valor);
			cursor.moveToPrevious();
		}
		u = String.format("%.2f", preco); // VALOR DAS RECEITAS
		return u;
	}

	// ------------METODOS QUE ALTERAM VALORES NO BANCO DE DADOS

	public boolean mudaNomeItem(long idLong, String item, String cesta) {
		ContentValues cv = new ContentValues();
		
		if (cesta.equals("vazia")) {
			cv.put(COLUNA_DESCRICAO_ITEM_LISTA, item);
			return this.db.update(TABELA_ITENS_LISTA, cv, COLUNA_ID_ITEM_LISTA
					+ " = '" + idLong + "'", null) > 0;
		} else {
			cv.put(COLUNA_DESCRICAO_ITEM_CESTA, item);
			return this.db.update(TABELA_ITENS_CESTA, cv, COLUNA_ID_ITEM_CESTA
					+ " = '" + idLong + "'", null) > 0;
		}
	}

	public boolean mudaNomeLista(String nomeAntes, String nomeDepois) {
		ContentValues cv = new ContentValues();
		nomeAntes = nomeAntes.replace("'", "''");
		cv.put(COLUNA_DESCRICAO_LISTA, nomeDepois);
		return this.db.update(TABELA_LISTAS, cv, COLUNA_DESCRICAO_LISTA
				+ " = '" + nomeAntes + "' ", null) > 0;
	}

	public boolean mudaNomeListaItens(String nomeAntes, String nomeDepois) {
		ContentValues cv = new ContentValues();
		nomeAntes = nomeAntes.replace("'", "''");
		cv.put(COLUNA_CODIGO_LISTA, nomeDepois);
		return this.db.update(TABELA_ITENS_LISTA, cv, COLUNA_CODIGO_LISTA
				+ " = '" + nomeAntes + "' ", null) > 0;
	}

	public boolean mudaPrecoItem(long idLong, String preco, String cesta) {
		ContentValues cv = new ContentValues();
		if (cesta.equals("vazia")) {
			cv.put(COLUNA_PRECO_ITEM_LISTA, preco);
			return this.db.update(TABELA_ITENS_LISTA, cv, COLUNA_ID_ITEM_LISTA
					+ " = '" + idLong + "'", null) > 0;
		} else {
			cv.put(COLUNA_PRECO_ITEM_CESTA, preco);
			return this.db.update(TABELA_ITENS_CESTA, cv, COLUNA_ID_ITEM_CESTA
					+ " = '" + idLong + "'", null) > 0;
		}
	}

	public boolean mudaQuantidadeItem(long idLong, String quantidade,
			String cesta) {
		ContentValues cv = new ContentValues();
		if (cesta.equals("vazia")) {
			cv.put(COLUNA_QUANTIDADE_ITEM_LISTA, quantidade);
			return this.db.update(TABELA_ITENS_LISTA, cv, COLUNA_ID_ITEM_LISTA
					+ " = '" + idLong + "'", null) > 0;
		} else {
			cv.put(COLUNA_QUANTIDADE_ITEM_CESTA, quantidade);
			return this.db.update(TABELA_ITENS_CESTA, cv, COLUNA_ID_ITEM_CESTA
					+ " = '" + idLong + "'", null) > 0;
		}
	}

	// ------------METODOS QUE COPIAM VALORES DO BANCO DE DADOS

	public String pegaItemCesta(String lista) throws SQLException {
		lista = lista.replace("'", "''");
		Cursor cursor = this.db.query(TABELA_ITENS_CESTA, colunas_itens_cesta,
				COLUNA_CODIGO_CESTA + " = '" + lista + "' ", null, null, null,
				null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(2);
		}
		cursor.close();
		return null;
	}

	public String pegaItemLista(String lista) throws SQLException {
		lista = lista.replace("'", "''");
		Cursor cursor = this.db.query(TABELA_ITENS_LISTA, colunas_itens_lista,
				COLUNA_CODIGO_LISTA + " = '" + lista + "' ", null, null, null,
				null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(2);
		}
		cursor.close();
		return null;
	}

	public String pegaItensLista(String lista) throws SQLException {
		lista = lista.replace("'", "''");
		Cursor cursor = this.db.query(TABELA_ITENS_LISTA, colunas_itens_lista,
				COLUNA_CODIGO_LISTA + " = '" + lista + "' ", null, null, null,
				COLUNA_DESCRICAO_ITEM_LISTA + " ASC");
		int i = cursor.getColumnIndex("item_lista");
		int j = cursor.getColumnIndex("qte_item_lista");
		String str = "Lista de itens de " + lista + ":\n";
		cursor.moveToFirst();
		while (true) {
			if (cursor.isAfterLast())
				return str;
			str = str + cursor.getString(j) + " de " + cursor.getString(i)
					+ ";\n";
			cursor.moveToNext();
		}
	}

	public String pegaLista() throws SQLException {
		Cursor cursor = this.db.query(true, TABELA_LISTAS, colunas_listas,
				null, null, null, null, null, null);
		if (cursor != null && cursor.moveToFirst()) {
			return cursor.getString(1);
		}
		cursor.close();
		return null;
	}

	// CLASSE DO BANCO DE DADOS QUE CRIA TABELAS OU ATUALIZA A VERSAO DO BANCO

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context pContext) {
			super(pContext, BANCO_DE_DADOS, null, VERSAO_BANCO_DE_DADOS);
		}

		public void onCreate(SQLiteDatabase pSQLiteDB) {

			// CRIA AS TABELAS DO BANCO DE DADOS
			pSQLiteDB.execSQL(CRIA_TABELA_LISTA);
			pSQLiteDB.execSQL(CRIA_TABELA_ITENS_LISTA);
			pSQLiteDB.execSQL(CRIA_TABELA_ITENS_CESTA);
			Log.w(TAG, "DB criado com sucesso!");
		}

		public void onUpgrade(SQLiteDatabase pSQLiteDB, int ver1, int ver2) {

			// METODO PARA MODIFICAR BANCO DE DADOS SEM PERDER INFORMACOES
			Log.w(TAG, "Atualizando o banco de dados da versao " + ver1
					+ " para " + ver2 + ", os dados serao apagados!");

			pSQLiteDB.execSQL("DROP TABLE IF EXISTS " + TABELA_LISTAS);
			pSQLiteDB.execSQL("DROP TABLE IF EXISTS " + TABELA_ITENS_LISTA);
			pSQLiteDB.execSQL("DROP TABLE IF EXISTS " + TABELA_ITENS_CESTA);

		}
	}

	// ----- Procedimentos para fazer BACKUP e RESTARURAR BACKUP

	@SuppressWarnings("resource")
	public void copiaBD() {

		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			if (sd.canWrite()) {
				String currentDBPath = "//data//com.msk.superlista//databases//super_lista";
				String backupDBPath = "super_lista";
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(sd, backupDBPath);

				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(currentDB)
							.getChannel();
					FileChannel dst = new FileOutputStream(backupDB)
							.getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
			}
		} catch (Exception e) {
		}
	}

	@SuppressWarnings("resource")
	public void restauraBD() {

		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			if (sd.canWrite()) {
				String currentDBPath = "//data//com.msk.superlista//databases//super_lista";
				String backupDBPath = "super_lista";
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(sd, backupDBPath);

				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(backupDB)
							.getChannel();
					FileChannel dst = new FileOutputStream(currentDB)
							.getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
				}
			}
		} catch (Exception e) {
		}
	}

}
