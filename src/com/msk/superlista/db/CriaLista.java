package com.msk.superlista.db;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v7.app.ActionBar;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.msk.superlista.R;
import com.msk.superlista.SuperLista;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
@SuppressLint("InlinedApi")
public class CriaLista extends SuperLista implements View.OnClickListener {

	// ELEMENTSO DA TELA
	private ImageButton ibAdiciona, ibBusca, ibEnvia, ibExclui, ibLembrete;
	private LayoutInflater inflaterLista;
	private ListView listaCriada;
	private AutoCompleteTextView nomeNovoItem;
	private TextView nomeLista, nomeItem, semItem, preco, unidade;
	private View viewLista;

	// ELEMENTOS PARA MONTAR A LISTA
	private ArrayAdapter<String> adapItem;
	private SimpleCursorAdapter buscaItens;
	private Cursor itensParaLista = null;
	private Cursor itensParaCesta = null;

	// CLASSE COM BANCO DE DADOS
	DBListas dbListaCriada = new DBListas(this);

	// VARIAVEIS UTLIZADAS
	final static Calendar c = Calendar.getInstance();
	private long idItem;
	private int cesta = 0;
	private String conteudoLista, itemDaLista, listaFeita, outroItemNovo,
			quantidadeItem, valorItem, nItem, valItem, qteItem;

	ActionBar actionBar;
	
	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.minha_lista);
		//setTheme(android.R.style.Theme_Black);
		iniciar();
		usarActionBar();
		listaFeita = getIntent().getExtras().getString("lista");
		nomeLista.setText(listaFeita);
		RecuperaItens();
		FazLista();
		nomeLista.setOnClickListener(this);
		ibBusca.setOnClickListener(this);
		ibAdiciona.setOnClickListener(this);
		ibEnvia.setOnClickListener(this);
		ibLembrete.setOnClickListener(this);
		listaCriada.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1,
					int posicao, long arg3) {

				// METODO CLIQUE NO ITEM DA LISTA
				dbListaCriada.open();
				itensParaLista.moveToPosition(posicao);
				idItem = itensParaLista.getLong(0);
				String str = dbListaCriada.mostraItemLista(idItem);
				dbListaCriada.close();
				FazLista();
				Bundle carta = new Bundle();
				carta.putString("item", str);
				carta.putLong("id", idItem);
				carta.putString("lista", listaFeita);
				Intent correio = new Intent("com.msk.superlista.EDITAITEM");
				correio.putExtras(carta);
				startActivity(correio);
			}

		});
	}

	private void iniciar() {
		nomeNovoItem = ((AutoCompleteTextView) findViewById(R.id.etItem));
		nomeLista = ((TextView) findViewById(R.id.tvNomeLista));
		nomeItem = ((TextView) findViewById(R.id.tvItemEscolhido));
		semItem = ((TextView) findViewById(R.id.tvSemItem));
		unidade = ((TextView) findViewById(R.id.tvUnidade));
		preco = ((TextView) findViewById(R.id.tvValor));
		listaCriada = ((ListView) findViewById(R.id.lvListaCriada));
		ibExclui = ((ImageButton) findViewById(R.id.ibExcluiItemEscolhido));
		ibBusca = ((ImageButton) findViewById(R.id.ibBuscaItens));
		ibAdiciona = ((ImageButton) findViewById(R.id.ibAdicionaItens));
		ibEnvia = ((ImageButton) findViewById(R.id.ibEnviaLista));
		ibLembrete = ((ImageButton) findViewById(R.id.ibNovoLembrete));
		adapItem = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, getResources()
						.getStringArray(R.array.ListaComTudo));
		nomeNovoItem.setAdapter(adapItem);

	}

	private void RecuperaItens() {
		// VERIFICA SE USUARIO DESEJA RECUPERAR ITENS DA CESTA
		dbListaCriada.open();
		cesta = dbListaCriada.contaItensCesta(listaFeita);
		dbListaCriada.close();

		if (cesta > 0) {
			// O QUE FAZER SE EXISTEM ITENS NA CESTA MAS NAO EXISTE NA LISTA
			new AlertDialog.Builder(new ContextThemeWrapper(this,
					R.style.TemaDialogo))
					.setTitle(R.string.titulo_recupera_lista)
					.setMessage(R.string.texto_recupera_lista)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface pDialogo,
										int pInt) {
									dbListaCriada.open();
									itensParaCesta = dbListaCriada
											.buscaItensCesta(listaFeita);
									for (int j = 0; j < itensParaCesta
											.getCount(); j++) {
										itensParaCesta.moveToPosition(j);
										nItem = itensParaCesta.getString(2);
										qteItem = itensParaCesta.getString(3);
										valItem = itensParaCesta.getString(4);
										dbListaCriada.insereItemLista(
												listaFeita, nItem, qteItem,
												valItem);
									}

									dbListaCriada.excluiItensCesta(listaFeita);
									buscaItens.notifyDataSetChanged();
									FazLista();
								}
							})
					.setNegativeButton("Cancelar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface pDialogo,
										int pInt) {
									pDialogo.dismiss();
								}
							}).show();

		}

	}

	@SuppressWarnings("deprecation")
	private void FazLista() {
		dbListaCriada.open();
		itensParaLista = dbListaCriada.buscaItensLista(listaFeita);
		int i = itensParaLista.getCount();
		dbListaCriada.close();
		if (i >= 0) {
			buscaItens = new SimpleCursorAdapter(this, R.id.lvListaCriada,
					itensParaLista, new String[] { "item_lista",
							"qte_item_lista", "preco_item_lista" },
					new int[] { R.id.tvItemEscolhido, R.id.tvUnidade,
							R.id.tvValor }) {

				// Metodo que monta a lista
				public View getView(int pItemLista, View vLista,
						ViewGroup vGLista) {

					// MONTA A LINHA DA LISTA
					viewLista = vLista;
					inflaterLista = getLayoutInflater();
					viewLista = inflaterLista.inflate(R.layout.item_escolhido,
							null);
					nomeItem = ((TextView) viewLista
							.findViewById(R.id.tvItemEscolhido));
					unidade = ((TextView) viewLista
							.findViewById(R.id.tvUnidade));
					preco = ((TextView) viewLista.findViewById(R.id.tvValor));
					ibExclui = ((ImageButton) viewLista
							.findViewById(R.id.ibExcluiItemEscolhido));
					ibExclui.setFocusable(false);
					// BUSCA VALORES PARA COLOCAR NA LINHA
					dbListaCriada.open();
					itensParaLista.moveToPosition(pItemLista);
					itemDaLista = itensParaLista.getString(2);
					quantidadeItem = itensParaLista.getString(3);
					valorItem = itensParaLista.getString(4);

					// COLOCA OS VALORES NA LINHA
					nomeItem.setText(itemDaLista);
					ibExclui.setTag(Long.valueOf(itensParaLista.getLong(0)));
					unidade.setText(quantidadeItem);
					preco.setText(valorItem);
					dbListaCriada.close();

					// METODO AO CLICAR NO ICONE DE EXCLUIR ITEM
					ibExclui.setOnClickListener(new View.OnClickListener() {
						public void onClick(View vItem) {
							ibExclui = ((ImageButton) vItem);
							idItem = ((Long) ibExclui.getTag()).longValue();
							dbListaCriada.open();
							String str = dbListaCriada.mostraItemLista(idItem);

							if (dbListaCriada.excluiItemLista(idItem) == true)
								Toast.makeText(
										getApplicationContext(),
										String.format(
												getResources()
														.getString(
																R.string.dica_item_removido),
												str), Toast.LENGTH_SHORT)
										.show();

							dbListaCriada.close();
							buscaItens.notifyDataSetChanged();
							FazLista();
						}
					});

					return viewLista;
				}

				// Metodo que conta a quantidade de itens na lista
				public int getCount() {
					dbListaCriada.open();
					int i = dbListaCriada.contaItensLista(listaFeita);
					dbListaCriada.close();
					return i;
				}

				// Metodo que entrega um numero para cada item da lista
				public long getItemId(int pItemLista) {
					return pItemLista;
				}

			};

			listaCriada.setAdapter(buscaItens);
			listaCriada.setEmptyView(semItem);
		}
	}

	public void onClick(View paramView) {
		switch (paramView.getId()) {
		case R.id.tvNomeLista:
			final EditText localEditText = new EditText(this);
			localEditText.setText(listaFeita);
			new AlertDialog.Builder(new ContextThemeWrapper(this,
					R.style.TemaDialogo))
					.setTitle(R.string.titulo_edita_nome)
					.setMessage(R.string.texto_edita_nome)
					.setView(localEditText)
					.setNeutralButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface paramAnonymousDialogInterface,
										int paramAnonymousInt) {
									String str = localEditText.getText()
											.toString();

									if (!listaFeita.equals(str)) {

										String nomeLista1 = str;
										String nomeLista2 = str;
										dbListaCriada.open();
										int a = dbListaCriada
												.contaNomeListas(nomeLista1);
										int b = 1;

										if (a != 0) {
											while (a != 0) {
												nomeLista2 = nomeLista1 + b;
												a = dbListaCriada
														.contaNomeListas(nomeLista2);
												b = b + 1;
											}
											str = nomeLista2;
										}
										nomeLista.setText(str);
										dbListaCriada.mudaNomeLista(listaFeita,
												str);
										dbListaCriada.mudaNomeListaItens(
												listaFeita, str);
										dbListaCriada.close();
										listaFeita = str;
										FazLista();
									}
								}
							}).show();

			break;
		case R.id.ibBuscaItens:
			String str = listaFeita;
			Bundle carta = new Bundle();
			carta.putString("lista", str);
			Intent envelope = new Intent("com.msk.superlista.COLOCAITENS");
			envelope.putExtras(carta);
			startActivity(envelope);
			break;
		case R.id.ibAdicionaItens:
			nomeNovoItem.setTag(nomeNovoItem.getText().toString());
			outroItemNovo = nomeNovoItem.getTag().toString();
			nomeNovoItem.setText("");
			if (!outroItemNovo.equals("")) {
				dbListaCriada.open();
				int qt = dbListaCriada.contaNomeItensListas(listaFeita,
						outroItemNovo);
				int qt0 = dbListaCriada.contaNomeItensCestas(listaFeita,
						outroItemNovo);
				if (qt == 0 && qt0 == 0) {
					if (dbListaCriada.insereItemLista(listaFeita,
							outroItemNovo,
							getResources().getString(R.string.dica_unid),
							getResources().getString(R.string.dica_zero)) >= 0)
						Toast.makeText(
								getApplicationContext(),
								String.format(
										getResources().getString(
												R.string.dica_item_novo),
										outroItemNovo), Toast.LENGTH_SHORT)
								.show();
				} else {
					Toast.makeText(
							getApplicationContext(),
							String.format(
									getResources().getString(
											R.string.dica_item_existente),
									outroItemNovo), Toast.LENGTH_SHORT).show();
				}
				dbListaCriada.close();
			}

			break;
		case R.id.ibEnviaLista:
			dbListaCriada.open();
			conteudoLista = dbListaCriada.pegaItensLista(nomeLista.getText()
					.toString());
			dbListaCriada.close();
			Intent correio = new Intent("android.intent.action.SEND");
			correio.putExtra("android.intent.extra.SUBJECT", getResources()
					.getString(R.string.listas));
			correio.putExtra("android.intent.extra.TEXT", conteudoLista);
			correio.setType("*/*");
			startActivity(Intent.createChooser(correio, String
					.format(getResources().getString(R.string.compartilhar),
							listaFeita)));
			finish();
			break;
		case R.id.ibNovoLembrete:
			dbListaCriada.open();
			conteudoLista = dbListaCriada.pegaItensLista(nomeLista.getText()
					.toString());
			dbListaCriada.close();
			Intent evento = new Intent(Intent.ACTION_EDIT);
			evento.setType("vnd.android.cursor.item/event");
			evento.putExtra(Events.TITLE, nomeLista.getText().toString());
			evento.putExtra(Events.DESCRIPTION, conteudoLista);

			evento.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
					c.getTimeInMillis());
			evento.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
					c.getTimeInMillis());

			evento.putExtra(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
			evento.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
			startActivity(evento);

			break;
		}
		buscaItens.notifyDataSetChanged();
		FazLista();
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) {
		super.onCreateOptionsMenu(pMenu);
		pMenu.clear();
		getMenuInflater().inflate(R.menu.menu_app, pMenu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		case R.id.ajustes:
			startActivity(new Intent("com.msk.superlista.AJUSTES"));
			break;
		case R.id.sobre:
			startActivity(new Intent("com.msk.superlista.SOBRE"));
			break;
		}
		return false;

	}
	
	protected void onRestart() {
		super.onRestart();
		FazLista();
	}

	protected void onDestroy() {

		dbListaCriada.open();
		int lista = dbListaCriada.contaItensLista(listaFeita);
		int cesta = dbListaCriada.contaItensCesta(listaFeita);

		if (lista <= 0 && cesta <= 0) {
			dbListaCriada.excluiLista(listaFeita);
		}
		dbListaCriada.close();
		super.onDestroy();
	}

	protected void onResume() {
		super.onResume();
		FazLista();
	}
}
