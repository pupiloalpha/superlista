package com.msk.superlista.db;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.msk.superlista.R;
import com.msk.superlista.SuperLista;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
@SuppressLint("InlinedApi")
public class EditaItem extends SuperLista implements
		OnItemSelectedListener, OnClickListener {

	// ELEMENTOS DA TELA
	private TextView dadosItem;
	private static TextView data, horario;
	private Button mudaItem;
	private Spinner unidades;
	private EditText valor, quantidade;
	private CheckBox calendario;

	// CLASSE COM BANCO DE DADOS
	DBListas dbListaCriada = new DBListas(this);

	// VARIAVEIS UTILIZADAS
	private long idItem;
	private double preco;
	private String nomeItem, quantidadeItem, unidadeItem, valorItem;
	private static int mDia, mMes, mAno, mHora, mMinuto;

	final static Calendar c = Calendar.getInstance();
	DialogFragment dialogo;

	ActionBar actionBar;
	
	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.edita_item);
		Bundle localBundle = getIntent().getExtras();
		nomeItem = localBundle.getString("item");
		idItem = localBundle.getLong("id");
		iniciar();
		usarActionBar();
		mostradados();
		DataDeHoje();
		dadosItem.setOnClickListener(this);
		unidades.setOnItemSelectedListener(this);
		mudaItem.setOnClickListener(this);
		calendario.setOnClickListener(this);
		data.setOnClickListener(this);
		horario.setOnClickListener(this);
	}

	private void iniciar() {
		quantidade = ((EditText) findViewById(R.id.etQuantidadeItem));
		dadosItem = ((TextView) findViewById(R.id.tvNomeItem));
		unidades = ((Spinner) findViewById(R.id.spUnidadeItem));
		valor = ((EditText) findViewById(R.id.etValorItem));
		mudaItem = ((Button) findViewById(R.id.btModificaItemLista));
		data = (TextView) findViewById(R.id.tvDataAlarme);
		horario = (TextView) findViewById(R.id.tvHorarioAlarme);
		calendario = (CheckBox) findViewById(R.id.cbCalendario);
		data.setVisibility(View.INVISIBLE);
		horario.setVisibility(View.INVISIBLE);
	}

	private void mostradados() {
		dadosItem.setText(String.format(
				getResources().getString(R.string.info_item), nomeItem));

		// BUSCA DADOS
		dbListaCriada.open();
		quantidadeItem = dbListaCriada.mostraQuantidadeItemLista(idItem);
		valorItem = dbListaCriada.mostraValorItemLista(idItem);
		dbListaCriada.close();

		// CONVERTE VALOR EM DECIMAL
		valorItem = valorItem.replace("R$ ", "");
		valorItem = valorItem.replace(",", ".");

		// COLOCA VALOR NA TELA
		if (!valorItem.equals("0.00"))
			valor.setText(valorItem);
		else
			valor.setText("");

		// DEFINE UNIDADE UTILIZADA
		int i = quantidadeItem.length();
		String unid = quantidadeItem.substring(i);

		if (unid.equals("c")) {
			unidades.setSelection(6);
		} else if (unid.equals("a")) {
			unidades.setSelection(1);
		} else if (unid.equals("g")) {
			unidades.setSelection(2);
		} else if (unid.equals("o")) {
			unidades.setSelection(3);
		} else if (unid.equals("g")) {
			unidades.setSelection(4);
		} else if (unid.equals("l")) {
			unidades.setSelection(5);
		} else {
			unidades.setSelection(0);
		}

		// CONVERTE QUANTIDADE EM NUMERO
		quantidadeItem = quantidadeItem.replace(" unid", "");
		quantidadeItem = quantidadeItem.replace(" caixa", "");
		quantidadeItem = quantidadeItem.replace(" kg", "");
		quantidadeItem = quantidadeItem.replace(" litro", "");
		quantidadeItem = quantidadeItem.replace(" g", "");
		quantidadeItem = quantidadeItem.replace(" ml", "");
		quantidadeItem = quantidadeItem.replace(" pc", "");

		// COLOCAR QUANTIDADE NA TELA
		if (!quantidadeItem.equals("01"))
			quantidade.setText(quantidadeItem);
		else
			quantidade.setText("");

	}

	private void DataDeHoje() {
		mAno = c.get(1);
		mMes = c.get(2);
		mDia = c.get(5);
		String d = mDia + "";
		int mm = (mMes + 1);
		String me = mm + "";
		if (d.length() == 1)
			d = "0" + d;
		if (me.length() == 1)
			me = "0" + me;
		data.setText(getResources().getString(R.string.dica_dia_alarme, d, me,
				mAno));
		mHora = c.get(Calendar.HOUR_OF_DAY);
		mMinuto = c.get(Calendar.MINUTE);
		String h = mHora + "";
		String m = mMinuto + "";
		if (h.length() == 1)
			h = "0" + h;
		if (m.length() == 1)
			m = "0" + m;
		horario.setText(getResources().getString(R.string.dica_horario_alarme,
				h, m));
	}

	public void onClick(View botao) {

		switch (botao.getId()) {

		case R.id.tvNomeItem:
			final EditText localEditText = new EditText(this);
			localEditText.setText(nomeItem);
			new AlertDialog.Builder(this)
					.setTitle(
							getResources().getString(R.string.titulo_nome_item))
					.setMessage(
							getResources().getString(R.string.texto_edita_item))
					.setView(localEditText)
					.setNeutralButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface paramAnonymousDialogInterface,
										int paramAnonymousInt) {
									String str = localEditText.getText()
											.toString();
									dbListaCriada.open();
									dbListaCriada.mudaNomeItem(idItem, str, "vazia");
									dbListaCriada.close();
									nomeItem = str;
									dadosItem.setText(nomeItem);
								}
							}).show();
			break;
		case R.id.cbCalendario:
			
			if (calendario.isChecked()) {
				data.setVisibility(View.VISIBLE);
				horario.setVisibility(View.VISIBLE);
			} else {
				data.setVisibility(View.INVISIBLE);
				horario.setVisibility(View.INVISIBLE);
			}
			
			break;
		case R.id.tvDataAlarme:

			dialogo = new AlteraData();
			dialogo.show(getSupportFragmentManager(), "alteraData");

			break;
		case R.id.tvHorarioAlarme:

			dialogo = new AlteraHorario();
			dialogo.show(getSupportFragmentManager(), "alteraHorario");

			break;
		case R.id.btModificaItemLista:

			quantidadeItem = quantidade.getText().toString();
			// QUANTIDADE DO ITEM
			if (quantidadeItem.equals(""))
				quantidadeItem = "01";
			if (quantidadeItem.length() == 1)
				quantidadeItem = "0" + quantidadeItem;

			String str2 = quantidadeItem + " " + unidadeItem;
			dbListaCriada.open();
			dbListaCriada.mudaQuantidadeItem(idItem, str2, "vazia");
			dbListaCriada.close();

			valorItem = valor.getText().toString();
			// PRECO DO ITEM
			if (valorItem.equals(""))
				preco = 0.0D;
			else
				preco = Double.parseDouble(valorItem);

			String str3 = "R$ " + String.format("%.2f", preco);

			dbListaCriada.open();
			if (dbListaCriada.mudaPrecoItem(idItem, str3, "vazia") == true) {
				Toast.makeText(
						getApplicationContext(),
						String.format(
								getResources().getString(
										R.string.dica_item_modificado),
								nomeItem), Toast.LENGTH_SHORT).show();
			}
			dbListaCriada.close();

			if (calendario.isChecked()) {
				c.set(mAno, mMes, mDia, mHora, mMinuto);

				Intent intent = new Intent(Intent.ACTION_EDIT);
				intent.setType("vnd.android.cursor.item/event");
				intent.putExtra(Events.TITLE, nomeItem);
				intent.putExtra(Events.DESCRIPTION,
						"Evento criado no SuperLista");

				intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
						c.getTimeInMillis());
				intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
						c.getTimeInMillis());

				intent.putExtra(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
				intent.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
				startActivity(intent);
			}

			finish();
			break;
		}
	}

	public void onItemSelected(AdapterView<?> adapter, View pView,
			int paramInt, long paramLong) {

		switch (paramInt) {
		case 0:
			unidadeItem = "unid";
			break;
		case 1:
			unidadeItem = "caixa";
			break;
		case 2:
			unidadeItem = "kg";
			break;
		case 3:
			unidadeItem = "litro";
			break;
		case 4:
			unidadeItem = "g";
			break;
		case 5:
			unidadeItem = "ml";
			break;
		case 6:
			unidadeItem = "pc";
			break;
		}

	}

	public void onNothingSelected(AdapterView<?> paramAdapterView) {
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getSupportActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public static class AlteraData extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return new DatePickerDialog(getActivity(), this, mAno, mMes, mDia);
		}

		public void onDateSet(DatePicker view, int ano, int mes, int dia) {
			mAno = ano;
			mMes = mes;
			mDia = dia;

			String d = mDia + "";
			int mm = (mMes + 1);
			String m = mm + "";
			if (d.length() == 1)
				d = "0" + d;
			if (m.length() == 1)
				m = "0" + m;
			data.setText(getResources().getString(R.string.dica_dia_alarme, d,
					m, mAno));
		}
	}

	public static class AlteraHorario extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return new TimePickerDialog(getActivity(), this, mHora, mMinuto,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hora, int minuto) {

			mHora = hora;
			mMinuto = minuto;

			String h = mHora + "";
			String m = mMinuto + "";
			if (h.length() == 1)
				h = "0" + h;
			if (m.length() == 1)
				m = "0" + m;
			horario.setText(getResources().getString(
					R.string.dica_horario_alarme, h, m));
		}
	}
}
