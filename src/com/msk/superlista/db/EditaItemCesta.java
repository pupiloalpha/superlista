package com.msk.superlista.db;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.msk.superlista.R;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
@SuppressLint("InlinedApi")
public class EditaItemCesta extends Activity implements
		OnItemSelectedListener, OnClickListener {

	// ELEMENTOS DA TELA
	private TextView dadosItem;
	private Button mudaItem;
	private Spinner unidades;
	private EditText valor, quantidade;

	// CLASSE COM BANCO DE DADOS
	DBListas dbListaCriada = new DBListas(this);

	// VARIAVEIS UTILIZADAS
	private long idItem;
	private double preco;
	private String nomeItem, quantidadeItem, unidadeItem, valorItem, tipoCesta;

	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.novo_edita_item);
		Bundle localBundle = getIntent().getExtras();
		nomeItem = localBundle.getString("item");
		tipoCesta = localBundle.getString("cesta");
		idItem = localBundle.getLong("id");
		iniciar();
		mostradados(tipoCesta);
		dadosItem.setOnClickListener(this);
		unidades.setOnItemSelectedListener(this);
		mudaItem.setOnClickListener(this);
	}

	private void iniciar() {
		quantidade = ((EditText) findViewById(R.id.etQuantidadeItem));
		dadosItem = ((TextView) findViewById(R.id.tvNomeItem));
		unidades = ((Spinner) findViewById(R.id.spUnidadeItem));
		valor = ((EditText) findViewById(R.id.etValorItem));
		mudaItem = ((Button) findViewById(R.id.btModificaItemLista));
	}

	private void mostradados(String cesta) {
		dadosItem.setText(String.format(
				getResources().getString(R.string.info_item), nomeItem));

		// BUSCA DADOS
		dbListaCriada.open();
		if (cesta.equals("vazia")) {
		quantidadeItem = dbListaCriada.mostraQuantidadeItemLista(idItem);
		valorItem = dbListaCriada.mostraValorItemLista(idItem);
		} else {
			quantidadeItem = dbListaCriada.mostraQuantidadeItemCesta(idItem);
			valorItem = dbListaCriada.mostraValorItemCesta(idItem);
		}
		dbListaCriada.close();

		// CONVERTE VALOR EM DECIMAL
		valorItem = valorItem.replace("R$ ", "");
		valorItem = valorItem.replace(",", ".");

		// COLOCA VALOR NA TELA
		if (!valorItem.equals("0.00"))
			valor.setText(valorItem);
		else
			valor.setText("");

		// DEFINE UNIDADE UTILIZADA
		int i = quantidadeItem.length();
		String unid = quantidadeItem.substring(i);

		if (unid.equals("c")) {
			unidades.setSelection(6);
		} else if (unid.equals("a")) {
			unidades.setSelection(1);
		} else if (unid.equals("g")) {
			unidades.setSelection(2);
		} else if (unid.equals("o")) {
			unidades.setSelection(3);
		} else if (unid.equals("g")) {
			unidades.setSelection(4);
		} else if (unid.equals("l")) {
			unidades.setSelection(5);
		} else {
			unidades.setSelection(0);
		}

		// CONVERTE QUANTIDADE EM NUMERO
		quantidadeItem = quantidadeItem.replace(" unid", "");
		quantidadeItem = quantidadeItem.replace(" caixa", "");
		quantidadeItem = quantidadeItem.replace(" kg", "");
		quantidadeItem = quantidadeItem.replace(" litro", "");
		quantidadeItem = quantidadeItem.replace(" g", "");
		quantidadeItem = quantidadeItem.replace(" ml", "");
		quantidadeItem = quantidadeItem.replace(" pc", "");

		// COLOCAR QUANTIDADE NA TELA
		if (!quantidadeItem.equals("01"))
			quantidade.setText(quantidadeItem);
		else
			quantidade.setText("");

	}

	public void onClick(View botao) {

		switch (botao.getId()) {

		case R.id.btModificaItemLista:

			quantidadeItem = quantidade.getText().toString();
			// QUANTIDADE DO ITEM
			if (quantidadeItem.equals(""))
				quantidadeItem = "01";
			if (quantidadeItem.length() == 1)
				quantidadeItem = "0" + quantidadeItem;

			String str2 = quantidadeItem + " " + unidadeItem;
			dbListaCriada.open();
			dbListaCriada.mudaQuantidadeItem(idItem, str2, tipoCesta);
			dbListaCriada.close();

			valorItem = valor.getText().toString();
			// PRECO DO ITEM
			if (valorItem.equals(""))
				preco = 0.0D;
			else
				preco = Double.parseDouble(valorItem);

			String str3 = "R$ " + String.format("%.2f", preco);

			dbListaCriada.open();
			if (dbListaCriada.mudaPrecoItem(idItem, str3, tipoCesta) == true) {
				Toast.makeText(
						getApplicationContext(),
						String.format(
								getResources().getString(
										R.string.dica_item_modificado),
								nomeItem), Toast.LENGTH_SHORT).show();
			}
			dbListaCriada.close();
			setResult(RESULT_OK, null);
			finish();
			break;
		}
	}

	public void onItemSelected(AdapterView<?> adapter, View pView,
			int paramInt, long paramLong) {

		switch (paramInt) {
		case 0:
			unidadeItem = "unid";
			break;
		case 1:
			unidadeItem = "caixa";
			break;
		case 2:
			unidadeItem = "kg";
			break;
		case 3:
			unidadeItem = "litro";
			break;
		case 4:
			unidadeItem = "g";
			break;
		case 5:
			unidadeItem = "ml";
			break;
		case 6:
			unidadeItem = "pc";
			break;
		}

	}

	public void onNothingSelected(AdapterView<?> paramAdapterView) {
	}
}
