package com.msk.superlista.info;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.MenuItem;
import android.widget.Toast;

import com.msk.superlista.R;
import com.msk.superlista.db.DBListas;

public class Ajustes extends PreferenceActivity implements
		OnPreferenceClickListener {

	private Preference backup, restaura, apagatudo, versao;
	private CheckBoxPreference cesta;
	private PreferenceScreen prefs;
	private String chave, nrVersao;
	private PackageInfo info;

	ActionBar actionBar;
	
	DBListas dbMinhasListas = new DBListas(this);

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferencias);

		prefs = getPreferenceScreen();

		backup = (Preference) prefs.findPreference("backup");
		restaura = (Preference) prefs.findPreference("restaura");
		apagatudo = (Preference) prefs.findPreference("apagatudo");
		versao = (Preference) prefs.findPreference("versao");
		cesta = (CheckBoxPreference) prefs.findPreference("cesta");

		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		nrVersao = info.versionName;

		versao.setSummary(getResources().getString(
				R.string.pref_descricao_versao, nrVersao));

		if (cesta.isChecked()) {
			cesta.setSummary(R.string.pref_descricao_cesta);
		} else {
			cesta.setSummary(R.string.pref_descricao_lista);
		}

		usarActionBar();
		
		backup.setOnPreferenceClickListener(this);
		restaura.setOnPreferenceClickListener(this);
		apagatudo.setOnPreferenceClickListener(this);
		cesta.setOnPreferenceClickListener(this);
	}

	public boolean onPreferenceClick(Preference itemPref) {

		chave = itemPref.getKey();

		if (chave.equals("backup")) {
			// Cria um Backup do Banco de Dados
			dbMinhasListas.open();
			dbMinhasListas.copiaBD();
			Toast.makeText(getApplicationContext(),
					getString(R.string.dica_copia_bd), Toast.LENGTH_SHORT)
					.show();
			dbMinhasListas.close();
		}
		if (chave.equals("restaura")) {
			// Restaura DB
			dbMinhasListas.open();
			dbMinhasListas.restauraBD();
			// onResume();
			Toast.makeText(getApplicationContext(),
					getString(R.string.dica_restaura_bd), Toast.LENGTH_SHORT)
					.show();
			dbMinhasListas.close();
		}
		if (chave.equals("apagatudo")) {
			// Apaga banco de dados
			dbMinhasListas.open();
			dbMinhasListas.excluiListas();
			// onResume();
			Toast.makeText(getApplicationContext(),
					getString(R.string.dica_exclusao_bd), Toast.LENGTH_SHORT)
					.show();
			dbMinhasListas.close();
		}
		if (chave.equals("cesta")){
			if (cesta.isChecked()) {
				cesta.setSummary(R.string.pref_descricao_cesta);
			} else {
				cesta.setSummary(R.string.pref_descricao_lista);
			}

		}
		return false;
	}
	
	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}