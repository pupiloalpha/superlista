package com.msk.superlista.info;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.widget.TextView;

import com.msk.superlista.R;

public class sobre extends Activity {
	
	private TextView sobre;
	private PackageInfo pinfo;
	private String versao;
		
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.sobre);
		
		 sobre = (TextView) findViewById(R.id.tvSobre);
			
		 try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 versao = pinfo.versionName;
		 
		 sobre.setText(getResources().getString(R.string.texto_sobre, versao));
		
		
	}
}
