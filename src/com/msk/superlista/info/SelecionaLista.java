package com.msk.superlista.info;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.support.v7.app.ActionBar;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.msk.superlista.R;
import com.msk.superlista.SuperLista;
import com.msk.superlista.db.DBListas;

@SuppressLint("InlinedApi")
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class SelecionaLista extends SuperLista {
	private SimpleCursorAdapter buscaListas;
	private DBListas dbListasCriadas = new DBListas(this);
	private ImageView ivApaga;
	private LayoutInflater inflaterLista;
	private String listaFeita, valor, qtItens, conteudoLista, nItem, qteItem,
			valItem, listaCopia;
	private Cursor listas = null;
	private Cursor itens = null;
	private ListView listasFeitas;
	private TextView nomeLista, qtItensLista, valorLista, listaVazia;
	private View viewLista;

	Calendar c = Calendar.getInstance();

	ActionBar actionBar;
	Intent correio;

	@Override
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.seleciona_lista);
		
		listasFeitas = ((ListView) findViewById(R.id.lvListas));
		listaVazia = (TextView) findViewById(R.id.tvListaVazia);
		FazLista();
		usarActionBar();

	}

	@SuppressWarnings("deprecation")
	private void FazLista() {
		dbListasCriadas.open();
		listas = dbListasCriadas.buscaListas();
		int i = listas.getCount();
		dbListasCriadas.close();
		if (i >= 0) {
			buscaListas = new SimpleCursorAdapter(this, R.id.lvListas, listas,
					new String[] { "lista" }, new int[] { R.id.tvListasFeitas }) {
				public int getCount() {
					dbListasCriadas.open();
					int i = dbListasCriadas.contaListas();
					dbListasCriadas.close();
					return i;
				}

				public long getItemId(int pInt) {
					return pInt;
				}

				public View getView(int pInt, View paramAnonymousView,
						ViewGroup paramAnonymousViewGroup) {
					viewLista = paramAnonymousView;
					inflaterLista = getLayoutInflater();
					viewLista = inflaterLista.inflate(R.layout.lista_para_uso,
							null);
					nomeLista = ((TextView) viewLista
							.findViewById(R.id.tvNomeLista));
					qtItensLista = ((TextView) viewLista
							.findViewById(R.id.tvItensLista));
					valorLista = ((TextView) viewLista
							.findViewById(R.id.tvValorLista));
					ivApaga = ((ImageView) viewLista
							.findViewById(R.id.ivApagaLista));
					ivApaga.setFocusable(false);
					dbListasCriadas.open();
					listas.moveToPosition(pInt);
					listaFeita = listas.getString(1);
					nomeLista.setText(listaFeita);
					int qt = dbListasCriadas.contaItensLista(listaFeita);
					qtItens = qt + "";
					if (qtItens.length() == 1)
						qtItens = "0" + qtItens;
					qtItensLista.setText(qtItens + " itens");
					// ADICIONAR STRINGS PARA QUANTIDADE DE ITENS DA LISTA

					valor = dbListasCriadas.mostraValorCesta(listaFeita);

					valorLista.setText(getResources().getString(
							R.string.dica_valor_lista, valor));

					viewLista.setTag(listaFeita);
					ivApaga.setTag(listaFeita);
					dbListasCriadas.close();

					ivApaga.setOnClickListener(new View.OnClickListener() {
						public void onClick(View pView) {
							ivApaga = ((ImageView) pView);
							String str = ivApaga.getTag().toString();
							dbListasCriadas.open();
							dbListasCriadas.excluiLista(str);
							dbListasCriadas.excluiItensLista(str);
							dbListasCriadas.close();
							Toast.makeText(
									getApplicationContext(),
									String.format(
											getResources()
													.getString(
															R.string.dica_lista_removida),
											str), Toast.LENGTH_SHORT).show();
							FazLista();
						}
					});
					
					registerForContextMenu(viewLista);
					
					viewLista.setOnClickListener(new View.OnClickListener() {
						
						public void onClick(View v) {
							// METODO QUANDO CLICAR NO NOME DA LISTA
														
							Bundle envelope = new Bundle();
							envelope.putString("lista", v.getTag().toString());
							correio = new Intent("com.msk.superlista.USALISTA");
							correio.putExtras(envelope);
							startActivity(correio);
							
						}
					});
					
					return viewLista;
				}
			};
			listasFeitas.setAdapter(buscaListas);
			listasFeitas.setEmptyView(listaVazia);
		}
	}

	
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		
		super.onCreateContextMenu(menu, v, menuInfo);
		
		listaFeita = v.getTag().toString();
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_altera_lista, menu);
		
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {	
		
		switch (item.getItemId()) {
		case R.id.botao_edita_lista:
			// Editar Lista
			Bundle envelope = new Bundle();
			envelope.putString("lista", listaFeita);
			Intent correio = new Intent(
					"com.msk.superlista.CRIALISTA");
			correio.putExtras(envelope);
			startActivity(correio);
			
			break;
		case R.id.botao_envia_lista:
			dbListasCriadas.open();
			conteudoLista = dbListasCriadas
					.pegaItensLista(listaFeita);
			dbListasCriadas.close();
			correio = new Intent(
					"android.intent.action.SEND");
			correio.putExtra(
					"android.intent.extra.SUBJECT",
					getResources().getString(
							R.string.listas));
			correio.putExtra("android.intent.extra.TEXT",
					conteudoLista);
			correio.setType("*/*");
			startActivity(Intent.createChooser(correio,
					String.format(
							getResources().getString(
									R.string.compartilhar),
							listaFeita)));
			
			break;
		case R.id.botao_copia_lista:
			Dialogo(listaFeita);
			
			break;
		case R.id.botao_lembrete_lista:
			dbListasCriadas.open();
			conteudoLista = dbListasCriadas
					.pegaItensLista(listaFeita);
			dbListasCriadas.close();
			Intent evento = new Intent(Intent.ACTION_EDIT);
			evento.setType("vnd.android.cursor.item/event");
			evento.putExtra(Events.TITLE, listaFeita);
			evento.putExtra(Events.DESCRIPTION,
					conteudoLista);

			evento.putExtra(
					CalendarContract.EXTRA_EVENT_BEGIN_TIME,
					c.getTimeInMillis());
			evento.putExtra(
					CalendarContract.EXTRA_EVENT_END_TIME,
					c.getTimeInMillis());

			evento.putExtra(Events.ACCESS_LEVEL,
					Events.ACCESS_PRIVATE);
			evento.putExtra(Events.AVAILABILITY,
					Events.AVAILABILITY_BUSY);
			startActivity(evento);
			
			break;

		}

		return super.onContextItemSelected(item);
	}


	private void Dialogo(String listacopia) {

		listaFeita = listacopia;
		final EditText localEditText = new EditText(this);
		localEditText.setText(listaFeita);
		new AlertDialog.Builder(new ContextThemeWrapper(this,
				R.style.TemaDialogo))
				.setTitle(R.string.titulo_edita_nome)
				.setMessage(R.string.texto_edita_nome)
				.setView(localEditText)
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface pDialogo,
									int pInt) {
								dbListasCriadas.open();
								itens = dbListasCriadas
										.buscaItensLista(listaFeita);
								listaCopia = localEditText.getText().toString();

								if (listaFeita.equals(listaCopia)) {

									String nomeLista1 = listaCopia;
									String nomeLista2 = listaCopia;
									int a = dbListasCriadas
											.contaNomeListas(nomeLista1);
									int b = 1;

									if (a != 0) {
										while (a != 0) {
											nomeLista2 = nomeLista1 + b;
											a = dbListasCriadas
													.contaNomeListas(nomeLista2);
											b = b + 1;
										}
										listaCopia = nomeLista2;
									}
								}

								dbListasCriadas.criaLista(listaCopia);

								for (int j = 0; j < itens.getCount(); j++) {
									itens.moveToPosition(j);
									nItem = itens.getString(2);
									qteItem = itens.getString(3);
									valItem = itens.getString(4);
									dbListasCriadas.insereItemLista(listaCopia,
											nItem, qteItem, valItem);
								}
								dbListasCriadas.close();
								buscaListas.notifyDataSetChanged();
								FazLista();
							}
						})
				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface pDialogo,
									int pInt) {
								pDialogo.dismiss();
								FazLista();
							}
						}).show();
	}

	@SuppressLint("NewApi")
	private void usarActionBar() {
		// Verifica a versao do Android para usar o ActionBar
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
			listasFeitas.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) {
		super.onCreateOptionsMenu(pMenu);
		pMenu.clear();
		getMenuInflater().inflate(R.menu.menu_app, pMenu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pMenuItem) {
		switch (pMenuItem.getItemId()) {

		case android.R.id.home:
			finish();
			break;
		case R.id.ajustes:
			startActivity(new Intent("com.msk.superlista.AJUSTES"));
			break;
		case R.id.sobre:
			startActivity(new Intent("com.msk.superlista.SOBRE"));
			break;
		}
		return false;

	}

	protected void onRestart() {
		super.onRestart();
		FazLista();
	}
}
